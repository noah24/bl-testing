.code16
.org 0

.section .text
.global _start
_start:

/* disable interrupts during init */
cli
/* stack starting at 0000:6F00 */
mov $0x6f00, %sp
/* store the drive number */
mov %dl, drive_number
/* re-enable interrupts */
sti

call ReadSector

movw $HelloWorldStr, %si
call PrintString

mov 0x8000, %cx
call PrintHex
call CRLF



l:
    jmp l



PrintChar:
    mov $0x0e, %ah
    mov $0, %bh
    mov $0x07, %bl
    int $0x10
    ret

PrintString:
    next_char:
        mov (%si), %al
        inc %si
        or %al, %al
        jz end_f
        call PrintChar
        jmp next_char

    end_f:
    ret

/* Useful for early debugging */
PrintHex:
    mov $'0', %al
    call PrintChar
    mov $'x', %al
    call PrintChar

    xor %si, %si
    loop1:
        inc %si
        mov %ch, %al
        and $0xf0, %al
        shr $0x04, %al
        add $0x30, %al

        cmp $0x39, %al
        ja loop1_alpha
        jmp loop1_printchar
        loop1_alpha:
        add $0x07, %al

        loop1_printchar:
        call PrintChar

        shl $0x04, %cx

        cmp $0x04, %si
        jb loop1

    ret


CRLF:
    movw $CrLfStr, %si
    call PrintString
    ret


ReadSector:
    mov $0x02, %ah /* read sector function id       */
    mov $0x01, %al /* num sectors to read           */
    mov $0x00, %dh /* disk head                     */
    mov drive_number, %dl /* pass the drive number  */
    mov $0x00, %ch /* cylinder number               */
    mov $0x02, %cl /* sector number to read         */
    mov $0x8000, %bx /* memory offset - load taget  */

    int $0x13

    jc ReadSector_Error
    ret

    ReadSector_Error:
    mov $DriveErrorStr, %si
    call PrintString
    ret


HelloWorldStr:
    .asciz "Hello, my friends. I am not a very good bootloader...\r\n"

CrLfStr:
    .asciz "\r\n"

DriveErrorStr:
    .asciz "Error reading drive\r\n"


drive_number:
    .word 0x0000



.fill 510-(.-_start), 1, 0
.word      0xaa55

.fill 512, 1, 0x11