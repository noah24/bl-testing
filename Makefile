AS = as
LD = ld

BOOTLOADER_SRC=src/boot.asm
BOOTLOADER_ORB=$(BOOTLOADER_SRC:.asm=.o)

BOOTLOADER=boot.bin

all: dirs bootloader

clean:
	rm -f ./**/*.o


%.o: %.asm
	$(AS) -o $@ -c $<

dirs:
	mkdir -p bin

bootloader: $(BOOTLOADER_ORB)
	$(LD) -o ./bin/$(BOOTLOADER) $^ -Ttext 0x7C00 --oformat=binary